import {Link} from "react-router-dom";

const Item = (props) => {
  const item = props.details.show;

  const {
    image,
    rating
  } = (item || {});
  
  return ( 
    <div className="grid-item">
      <Link to={`show/${item.id}`} id={item.id}>
        {/* <img rel="preload" src={item.image.medium} /> */}
        <img rel="preload" src="https://via.placeholder.com/150x200.png?text=Show%20Poster" />
        <h3>{item.name}</h3>
      </Link>
    </div>
   );
}
 
export default Item;